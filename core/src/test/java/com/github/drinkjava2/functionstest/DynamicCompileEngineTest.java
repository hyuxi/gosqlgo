package com.github.drinkjava2.functionstest;
 
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.gitee.drinkjava2.gosqlgo.compiler.DynamicCompileEngine;

/**
 * Dynamic Compile Engine Test
 * 
 * @author Yong Zhu
 * @since 1.7.0
 */
@SuppressWarnings("all")
public class DynamicCompileEngineTest {
	List l;
	
	static String javaSrc=
			"    package com.foo.bar;         \n" + 
			"	 import java.util.List;	      \n" + 
			"                                 \n" + 
			"	 public class DynaClass {     \n" + 
			"	   public String toString() { \n" + 
			"	     return  \"Test Dynamic Compile\"; \n" + 
			"		 } \n" + 
			"	 }";
	@Test
	public void doTest() throws IllegalAccessException, InstantiationException { 
		String fullName = "com.foo.bar.DynaClass"; 
		Class<?> clazz = null;
		for (int i = 0; i < 100000; i++)// Test class cached
			clazz = DynamicCompileEngine.instance.javaCodeToClass(fullName, javaSrc);
		Object instance = DynamicCompileEngine.instance.javaCodeToNewInstance(fullName, javaSrc);
		System.out.println(instance.toString());
		Assert.assertEquals(instance.toString(), "Test Dynamic Compile");
	}
	
}
