call mvn clean -DskipTests package
@echo off 

@echo on
cd target
del ROOT.war
ren *.war ROOT.war 

set WeblogicFolder=C:\Oracle\Middleware\Oracle_Home\user_projects\domains\base_domain

copy ROOT.war %WeblogicFolder%\autodeploy\ /y

call %WeblogicFolder%\startWebLogic.cmd

start http://127.0.0.1 