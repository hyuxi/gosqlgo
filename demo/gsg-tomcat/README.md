## gsg-tomcat
这是一个GoSqlGo在Tomcat环境下运行的演示项目。  
编译及运行本项目需Java8, 要求本机装有Tomcat7以上版本，必须发布到Tomcat目录下运行。  
 
运行方式：修改deploy_tomcat.bat批处理文本中的Tomcat目录,指向本机的Tomcat路径，双击运行即可。  
查看：浏览器输入 http://localhost   

顺便说一下，在Demo目录下还有一个gsg-springboot演示项目，演示了GoSqlGo在SpringBoot环境下的运行。