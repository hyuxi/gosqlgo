/* Copyright 2018-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by
 * applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */
package com.gitee.gosqlgo.demo;

import static com.github.drinkjava2.jsqlbox.JSQLBOX.gctx;

import javax.sql.DataSource;

import org.h2.jdbcx.JdbcConnectionPool;

import com.github.drinkjava2.jbeanbox.JBEANBOX;
import com.github.drinkjava2.jdialects.Dialect;
import com.github.drinkjava2.jsqlbox.SqlBoxContext;
import com.github.drinkjava2.jtransactions.tinytx.TinyTx;
import com.github.drinkjava2.jtransactions.tinytx.TinyTxConnectionManager;

/**
 * A Simple Demo application controller
 * 
 * @author Yong Zhu
 * @since 1.0.0
 */
public class Config {
	static DataSource ds = JdbcConnectionPool
			.create("jdbc:h2:mem:DBName;MODE=MYSQL;DB_CLOSE_DELAY=-1;TRACE_LEVEL_SYSTEM_OUT=0", "sa", "");

	static {
		SqlBoxContext.setGlobalNextDialect(Dialect.H2Dialect);
		SqlBoxContext ctx = new SqlBoxContext(ds);
		ctx.setConnectionManager(TinyTxConnectionManager.instance());// 事务相关
		SqlBoxContext.setGlobalSqlBoxContext(ctx);// 设定全局缺省上下文，给ActiveRecord用

		JBEANBOX.bctx().addContextAop(new TinyTx(ds), "*", "tx*");// 所有以tx打头的方法都启动事务

		for (String ddl : ctx.toCreateDDL(Account.class))// 第一次要建表
			gctx().nExecute(ddl);

		new Account().setId("A").setAmount(800).insert();// 第一次要准备测试数据
		new Account().setId("B").setAmount(300).insert();
	}

}
